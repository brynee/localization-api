﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace CultureLocalization.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IStringLocalizer<SharedResource> _localizer;
        private readonly IStringLocalizer<ValuesController> _aboutLocalizerizer;

        public ValuesController(IStringLocalizer<SharedResource> localizer, IStringLocalizer<ValuesController> aboutLocalizerizer)
        {
            _localizer = localizer;
            _aboutLocalizerizer = aboutLocalizerizer;
        }
        // GET api/values
        [HttpGet]
        public string Get()
        {
            return _aboutLocalizerizer["AboutTitle"];
        }
    }
}
