# Localization API

After git clone the project to your local,
1. Run the project. You will see the default content of message
2. Use query string '?culture={language_you_want}'. You will see different content.

In this case, if you choose different language, the content can change accordingly.
Language supported: 'en-US', 'de-CH', 'fr-CH', 'it-CH'